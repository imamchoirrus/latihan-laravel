<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function bio(){
        return view('halaman.form');
    }

    public function kirim(Request $request){
       $depan=$request['firstname'];
       $belakang=$request['lastname'];

       return view('halaman.home', compact('depan','belakang'));

    }
}
