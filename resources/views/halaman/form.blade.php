@extends('layout.master')

@section('judul')
Halaman Form
@endsection

@section('content')
<form action="/welcome" method="post">
    @csrf
        <div>
            <label for="nama">First name:</label><br><br>
            <input type="text" name="firstname">
        </div><br>
        
        <div>
            <label for="last">Last name:</label><br><br>
            <input type="text" name="lastname">
        </div> <br>
       
        <div>
            <label>Gander:</label><br><br>
            <input type="radio" name="gander" value="M"> Male <br>
            <input type="radio" name="gander" value="F"> Female <br>
            <input type="radio" name="gander" value="O"> Other <br>
        </div><br>
        
        <div>
            <label>Nationality:</label> <br><br>
            <select name="negara">
                <option value="Indonesia">Indonesia</option>
                <option value="Amerika">Amerika</option>
                <option value="Inggris">Inggris</option>
            </select>
        </div><br>

        <div>
            <label>Language Spoken:</label><br><br>
            <input type="checkbox" name="bahasa" value="Bahasa Indonesia"> Bahasa Indonesia <br>
            <input type="checkbox" name="bahasa" value="English"> English <br>
            <input type="checkbox" name="bahasa" value="Other"> Other <br>
        </div><br>

         <div>
            <label>Bio:</label><br><br>
            <textarea cols="30" rows="10"></textarea>
        </div><br>
        <input type="submit" value="Sign Up">
    </form>
@endsection